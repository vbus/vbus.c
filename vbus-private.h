#ifndef VBUS_PRIVATE_H
#define VBUS_PRIVATE_H

#include "vbus.h"
#include <nats/nats.h>
#include <jansson.h>
#include <string.h>
#include <pthread.h>

#define MAX_NUM_SUBSCRIBE   1024
#define RUNTIME_LOG
// nats.c
typedef struct ExtendedNatsClient_s ExtendedNatsClient;
struct ExtendedNatsClient_s  {
	const char *hostname;                   // client hostname
	const char *remoteHostname;             // remote client server hostname
	char *id;                         // app identifier
    char *idh;                         // app identifier
	char *configFile;                 // config folder root
	natsConnection      *client;        // client handle
    pthread_mutex_t mutex;               // secure message to nats
    char *sublist[MAX_NUM_SUBSCRIBE];
};

ExtendedNatsClient *NewExtendedNatsClient(char *domain, char *id);
bool ExtendedConnect(ExtendedNatsClient* c);
bool storeSubPath(ExtendedNatsClient* c, char *path);
bool checkSubPath(ExtendedNatsClient* c, char *path);

// node.c
struct Node_s
{
    char *path;
    Node *parent;
    json_t *element;
    ExtendedNatsClient *client;
    natsSubscription    *sub[4];
};


struct Attribute_s
{
    char *path;
    Node *parent;
    char *type;
    json_t *value;
    ExtendedNatsClient *client;
    natsSubscription    *sub;
};

struct Method_s
{
    char *path;
    Node *parent;
    json_t *element;
    ExtendedNatsClient *client;
    natsSubscription    *sub;
    method_cb cb;
    void *userdata;
};



Node *NewMasterNode( ExtendedNatsClient *nats);
bool NodeInitialize(Node *n);

// avahi-discovery.h
char *avahi_interface_init(void);

// helpers.c
const char * getHostname(void);
char * genPassword( void) ;
char* replace_char(char* str, char find, char replace);

#endif