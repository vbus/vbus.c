#include "vbus-private.h"
#include "crypt_blowfish-1.3/ow-crypt.h"
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const char bcrypt_setting[] = "$2a$11$CCCCCCCCCCCCCCCCCCCCC.";


ExtendedNatsClient *NewExtendedNatsClient(char *domain, char *id)
{
    printf("create vbus client service %s.%s\n", domain, id);
    fflush(stdout);
    // create nats struct
    ExtendedNatsClient *nats = (ExtendedNatsClient *)malloc(sizeof(ExtendedNatsClient));
    nats->hostname = getHostname();
    printf("hostname: %s\n", nats->hostname);
    fflush(stdout);
    nats->remoteHostname = nats->hostname;
    printf("remoteHostname: %s\n", nats->remoteHostname);
    fflush(stdout);

    // generate service uuid
    nats->id = (char *)malloc(strlen(id) + strlen(domain) + 2);
    strcpy(nats->id, domain);
    strcat(nats->id, ".");
    strcat(nats->id, id);
    printf("id: %s\n", nats->id);
    fflush(stdout);

    nats->idh = (char *)malloc(strlen(id) + strlen(domain) + strlen(nats->hostname) + 3);
    strcpy(nats->idh, domain);
    strcat(nats->idh, ".");
    strcat(nats->idh, id);
    strcat(nats->idh, ".");
    strcat(nats->idh, nats->hostname);
    printf("idh: %s\n", nats->idh);
    fflush(stdout);

    // retreive configuration file path
    int homedir_strlen = strlen(nats->idh) + strlen(".conf") + 2;
    int addvbusfolder = 0;
    char *homedir = getenv("VBUS_PATH");
    if(homedir == NULL) {
        homedir = getenv("HOME");
        addvbusfolder = 1;
    }
    homedir_strlen += strlen(homedir);
    if(addvbusfolder)
        homedir_strlen += strlen("/vbus/") ;
    nats->configFile = (char *)malloc(homedir_strlen);
    strcpy(nats->configFile, homedir);
    if(addvbusfolder)
        strcat(nats->configFile, "/vbus/");
    mkdir(nats->configFile, 0666);
    strcat(nats->configFile, ".");
    strcat(nats->configFile, nats->idh);
    strcat(nats->configFile, ".conf");
    printf("config file: %s\n", nats->configFile);
    fflush(stdout);

    int i;
    for(i=0; i<MAX_NUM_SUBSCRIBE;  i++)
    {
        nats->sublist[i] = NULL;
    }

    pthread_mutex_init(&nats->mutex, NULL);

    return nats;
}


// Creates a default configuration object.
json_t *getDefaultConfig(ExtendedNatsClient *c)
{
    json_t *config_j = json_object();

    // create password
    const char *private_key = genPassword();
    const char *public_key = crypt(private_key, bcrypt_setting);

    // create user
    json_t *client_j = json_object();
    json_object_set_new(client_j, "user", json_string(c->idh));
    json_object_set_new(client_j, "password", json_string((const char *)public_key));

    char *idplus = (char *)malloc(strlen(c->id)+strlen(".>")+1);
    strcpy(idplus, c->id);
    strcat(idplus, ".>");
    json_t *array_permission = json_object();
    json_t *array_pub = json_array();
    json_array_append(array_pub, json_string(c->id));
    json_array_append(array_pub, json_string(idplus));
    json_object_set( array_permission, "publish", array_pub);
    json_t *array_sub = json_array();
    json_array_append(array_sub, json_string(c->id));
    json_array_append(array_sub, json_string(idplus));
    json_object_set( array_permission, "subscribe", array_sub);
    json_object_set( client_j, "permissions", array_permission);
    json_object_set( config_j, "client", client_j);
    free(idplus);

    // create private information
    json_t *key_j = json_object();
    json_object_set_new(key_j, "private", json_string((const char *)private_key));
    json_object_set( config_j, "key", key_j);

    // create empty vbus information
    json_t *vbus_j = json_object();
    json_object_set( config_j, "vbus", vbus_j);

    char * res = json_dumps(config_j, JSON_ENCODE_ANY);
    printf ( "%s\n", res);
    free(res);
    
    return config_j;
}

// Try to read config file.
// If not found, it returns the default configuration.
json_t *readOrGetDefaultConfig(ExtendedNatsClient *c)
{
    printf("check if we already have a Vbus config file in %s", c->configFile);
    fflush(stdout);

    FILE* configfile = fopen(c->configFile, "rw");

    if (configfile == NULL) { /* file not already created */
        printf("vbus - create config file\n");
        fflush(stdout);
        return getDefaultConfig(c);
    }
    else
    {
        printf("vbus - load config file\n");
        fflush(stdout);
        json_error_t error;
        json_t *config = json_loadf(configfile, JSON_DECODE_ANY, &error);
        fclose(configfile);
        return config;
    }
}

// find vbus server  - strategy 1: get url from config file
char *getUrlFromConfig(json_t *config)
{
    char *url;
    printf("get URL from config\n");
    fflush(stdout);
    json_t *vbus_j = json_object_get( config, "vbus");
    if(vbus_j)
    {
        json_t *url_j = json_object_get( vbus_j, "url");
        if(url_j)
        {
            char *tmpurl = json_string_value(url_j);
            printf("found %s\n", tmpurl);
            fflush(stdout);
            url = strdup( tmpurl);
            // json_t *hostname_j = json_object_get( vbus_j, "hostname");
            // if(hostname_j)
            // {
            //     newHost = json_string_value(hostname_j);
            // }
        }
        else 
        {
            return NULL;
        }
    }
    else 
    {
        return NULL;
    }
    return url;
}

// find vbus server  - strategy 2: get url from ENV:VBUS_URL
char *getUrlFromEnv(json_t *config)
{
    char *url;
    printf("get URL from ENV\n");
    fflush(stdout);
    url = getenv("VBUS_URL");
    if((url!=NULL)&&(strlen(url)>3))
    {
        printf("found %s\n", url);
        fflush(stdout);
        return url;
    }
    return NULL;
}

// find vbus server - strategy 3: try usual docher host IP: 172.17.0.2
char *getUrlFromGenerate(int num)
{
    char cnum[4];
    sprintf(cnum, "%d", num);
    char *url = (char *)malloc(strlen("172.17.0.") + strlen(cnum) + strlen(":21400") + 1);
    strcpy(url, "172.17.0.");
    strcat(url, cnum);
    strcat(url, ":21400");
    printf("generate url %s\n", url);
    fflush(stdout);
    return url;
}

// find vbus server  - strategy 4: find it using avahi
char *getUrlFromAvahi(json_t *config)
{
    printf("get URL from avahi\n");
    fflush(stdout);
    return avahi_interface_init();
}

static int test_vbus_url( char *urltotest)
{
    int vbus_test = 0;
    natsConnection      *nc  = NULL;
    char *nats_path_tmp = malloc(strlen("nats://anonymous:anonymous@")  + strlen(urltotest) + 1);
    strcpy(nats_path_tmp, "nats://anonymous:anonymous@");
    strcat(nats_path_tmp, urltotest);
    printf("nats test: %s\n", nats_path_tmp);
    fflush(stdout);
    int s = natsConnection_ConnectTo(&nc, nats_path_tmp);
    if (s == NATS_OK) {
        printf("succedded with: %s\n", nats_path_tmp);
        //natsConnection_Flush(nc);
        vbus_test = 1;
        //natsConnection_Close(nc);
        natsConnection_Destroy(nc);
    }
    else {
        printf("fail with: %s - error: %d\n", nats_path_tmp, s);
    }
    fflush(stdout);
    free(nats_path_tmp);

    return vbus_test;
}

// get vBusURL from all different strategy
char *findvBusUrl(json_t *config)
{
    printf("start vBus discovery\n");
    fflush(stdout);
    char *vBusURL = getUrlFromConfig( config);
    if(vBusURL != NULL)
    {
        if(test_vbus_url(vBusURL))
        {
            printf("url from config ok: %s\n", vBusURL);
            fflush(stdout);
            return vBusURL;
        } else {
            printf("url from config hs: %s\n", vBusURL);
            fflush(stdout);
        }
    }

    vBusURL = getUrlFromEnv( config);
    if(vBusURL != NULL)
    {
        if(test_vbus_url(vBusURL))
        {
            printf("url from ENV ok: %s\n", vBusURL);
            fflush(stdout);
            return vBusURL;
        } else {
            printf("url from ENV hs: %s\n", vBusURL);
            fflush(stdout);
        }
    }

    int i;
    for(i=1; i<10; i++)
    {
        vBusURL = getUrlFromGenerate(i);
        if(vBusURL != NULL)
        {
            if(test_vbus_url(vBusURL))
            {
                printf("url from generation ok: %s\n", vBusURL);
                fflush(stdout);
                return vBusURL;
            } else {
                printf("url from generation hs: %s\n", vBusURL);
                fflush(stdout);
            }
            free(vBusURL);
            vBusURL = NULL;
        }
    }

    vBusURL = getUrlFromAvahi( config);
    if(vBusURL != NULL)
    {
        if(test_vbus_url(vBusURL))
        {
            printf("url from avahi ok: %s\n", vBusURL);
            fflush(stdout);
            return vBusURL;
        } else {
            printf("url from avahi hs: %s\n", vBusURL);
            fflush(stdout);
        }
    }

    return NULL;
}

static void saveConfigFile( char *filename, json_t *conf)
{
    printf("vbus - update config file\n");
    fflush(stdout);
    if( access( filename, F_OK ) != -1 ) {
        remove(filename);
    }
    json_dump_file( conf, filename, JSON_ENCODE_ANY);
}

bool publishUser(char *vBusUrl, json_t *config, char*hostname)
{
    natsConnection      *nc  = NULL;
    char *nats_path_anonymous = malloc(strlen("nats://anonymous:anonymous@")  + strlen(vBusUrl) + 1);
    strcpy(nats_path_anonymous, "nats://anonymous:anonymous@");
    strcat(nats_path_anonymous, vBusUrl);
    if (natsConnection_ConnectTo(&nc, nats_path_anonymous) == NATS_OK) {
        printf("publish user on %s\n", vBusUrl);
        fflush(stdout);
        char *pub_path = malloc(strlen("system.authorization.")  + strlen(hostname) + strlen(".add") + 1);
        strcpy(pub_path, "system.authorization.");
        strcat(pub_path, hostname);
        strcat(pub_path, ".add");
        json_t *client_j = json_object_get( config, "client");
        char * client_serialized = json_dumps(client_j, JSON_ENCODE_ANY);
        natsConnection_Publish(nc, pub_path, (const void*) client_serialized, strlen(client_serialized));
        sleep(1);
        natsConnection_Flush(nc);
        natsConnection_Close(nc);
        natsConnection_Destroy(nc);
        free(pub_path);
        free(client_serialized);
        free(nats_path_anonymous);
        return true;
    }
    else
    {
        printf("ERROR connecting to vbus server for user publish\n");
        fflush(stdout);
        free(nats_path_anonymous);
        return  false;
    }
    
}

bool ExtendedConnect(ExtendedNatsClient* c)
{
    json_t *config = readOrGetDefaultConfig(c);

    char *vBusUrl = findvBusUrl(config);
    if( vBusUrl == NULL)
    {
        return false;
    }

    json_t *vbus_j = json_object_get( config, "vbus");
    json_object_set( vbus_j, "url", json_string(vBusUrl));

    saveConfigFile(c->configFile, config);

    publishUser(vBusUrl, config, c->hostname);

    json_t *key_j = json_object_get( config, "key");
    json_t *private_j = json_object_get( key_j, "private");
    char *nats_path_full = malloc(strlen("nats://") + strlen(c->idh) + strlen(":") + strlen(json_string_value(private_j)) + strlen("@") + strlen(vBusUrl) + 1);
    strcpy(nats_path_full, "nats://");
    strcat(nats_path_full, c->idh);
    strcat(nats_path_full, ":");
    strcat(nats_path_full, json_string_value(private_j));
    strcat(nats_path_full, "@");
    strcat(nats_path_full, vBusUrl);
    if (natsConnection_ConnectTo(&(c->client), nats_path_full) == NATS_OK) 
    {
        printf("main: Start vbus on %s\n", vBusUrl);
        free(vBusUrl);
    }
    else
    {
        printf("ERROR main: connecting to vbus server\n");
        free(nats_path_full);
        free(vBusUrl);
        return  false;
    }

    return true;
}

bool storeSubPath(ExtendedNatsClient* c, char *path)
{
    int i= 0;
    while((c->sublist[i]!=NULL)&&(i<MAX_NUM_SUBSCRIBE))
    {
        i++;
    }

    if(i<MAX_NUM_SUBSCRIBE)
    {
        c->sublist[i] = path;
        return true;
    }

    return false;
}

bool checkSubPath(ExtendedNatsClient* c, char *path)
{
    int check;
    int i= 0;

    while((c->sublist[i]!=NULL)&&(i<MAX_NUM_SUBSCRIBE))
    {
        check = strcmp(c->sublist[i], path);
        if(check == 0)
        {
            return true;
        }
        i++;
    }
    return false;
}