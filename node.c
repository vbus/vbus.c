#include "vbus-private.h"

#include <string.h>


Node *NewMasterNode( ExtendedNatsClient *nats) 
{

    Node *node = (Node *)malloc(sizeof(Node));
    node->client = nats;
	node->path = (char *)malloc(strlen(nats->idh) + 1);
    strcpy(node->path, nats->idh);
    node->parent = NULL;
    node->element = json_object();

    return node;
}

static void MasterCb(natsConnection *nc, natsSubscription *sub, natsMsg *msg, void *closure)
{
    Node *n = (Node *)closure;
    const char *subject = natsMsg_GetSubject(msg);
    const char *data = natsMsg_GetData(msg);
#ifdef RUNTIME_LOG
    printf("Received msg: %s - %.*s\n", subject, natsMsg_GetDataLength(msg), data);
    fflush(stdout);
#endif
    pthread_mutex_lock(&n->client->mutex);
    
    if(checkSubPath(n->client, subject))
    {
#ifdef RUNTIME_LOG
        printf("path already sub specificly\n");
        fflush(stdout);
#endif
    }
    else if(strcmp(n->path,subject)==0)
    {
#ifdef RUNTIME_LOG
        printf("generate full tree for discover request\n");
        fflush(stdout);
#endif
        char * return_serialized = json_dumps(n->element, JSON_ENCODE_ANY);
        natsConnection_Publish(nc, natsMsg_GetReply(msg), (const void*) return_serialized, strlen(return_serialized));
        free(return_serialized);
    }
    else if(strncmp(n->path,subject,strlen(n->path))==0)
    {
        char *subpath = (char *)malloc(strlen(subject));
        strcpy(subpath, subject + strlen(n->path) + 1);
#ifdef RUNTIME_LOG
        printf("discover subpath %s\n", subpath);
        fflush(stdout);
#endif
        json_t *prev_j = NULL;
        json_t *key_j = n->element;
        char * prevToken = NULL;
        char * strToken = strtok ( subpath, "." );
        while ( strToken != NULL ) {
#ifdef RUNTIME_LOG
            printf("check substr %s\n", strToken);
            fflush(stdout);
#endif
            if(strcmp(strToken,"add") == 0)
            {
#ifdef RUNTIME_LOG
                printf ( "command .add found\n");
                fflush(stdout);
#endif
                json_error_t error;
                json_t *tmpadd = json_loads(data, JSON_DECODE_ANY, &error);
                if(tmpadd == NULL)
                {
#ifdef RUNTIME_LOG
                    printf ( "error decoding %s\n", data);
                    fflush(stdout);
#endif
                }
                else
                {
                    // extract each key and incrust is in local json
                    void *iter = json_object_iter(tmpadd);
                    while(iter)
                    {
                        const char *key = json_object_iter_key(iter);
                        json_t *value = json_object_iter_value(iter);
                        json_object_set( key_j, key, json_deep_copy(value));

                        iter = json_object_iter_next(tmpadd, iter);
                    }
                    char * res = json_dumps(key_j, JSON_ENCODE_ANY);
#ifdef RUNTIME_LOG
                    printf ( "result add: %s\n", res);
                    fflush(stdout);
#endif
                    free(res);
                    json_decref(tmpadd);
                }

                break;
            }
            else if(strcmp(strToken,"remove") == 0)
            {
#ifdef RUNTIME_LOG
                printf ( "command .remove found for %s\n", prevToken);
                fflush(stdout);
#endif
                json_object_clear(key_j);
                json_object_del(prev_j, prevToken);
                break;
            }
            else if(strcmp(strToken,"get") == 0)
            {
#ifdef RUNTIME_LOG
                printf ( "command .get found for %s\n", prevToken);
                fflush(stdout);
#endif
                char * return_serialized = json_dumps(key_j, JSON_ENCODE_ANY);
                natsConnection_Publish(nc, natsMsg_GetReply(msg), (const void*) return_serialized, strlen(return_serialized));
                free(return_serialized);
                break;
            }
            else if(strcmp(strToken,"set") == 0)
            {
#ifdef RUNTIME_LOG
                printf ( "command .set found\n");
                fflush(stdout);
#endif
                json_error_t error;
                json_t *tmpset = json_loads(data, JSON_DECODE_ANY, &error);
                if(tmpset == NULL)
                {
#ifdef RUNTIME_LOG
                    printf ( "error decoding %s\n", data);
                    fflush(stdout);
#endif
                }
                else
                {
                    if(strcmp(prevToken,"value") == 0)
                    {
#ifdef RUNTIME_LOG
                        printf ( "update  attribute %s\n", data);
                        fflush(stdout);
#endif
                        json_t *updatevalue = json_object();
                        json_object_set( updatevalue, "value", tmpset);
                        int ret = json_object_update(prev_j, updatevalue);
                        if(ret != 0)
                        {
#ifdef RUNTIME_LOG
                            printf ( "update attribute failed\n");
                            fflush(stdout);
#endif
                        }
                    }
                    else
                    {
#ifdef RUNTIME_LOG
                        printf("set request not handled yet\n");
                        fflush(stdout);
#endif
                    }
                    // extract each key and incrust is in local json
                    char * res = json_dumps(prev_j, JSON_ENCODE_ANY);
#ifdef RUNTIME_LOG
                    printf ( "result add: %s\n", res);
                    fflush(stdout);
#endif
                    free(res);
                    json_decref(tmpset);
                }

                break;
            }
            
#ifdef RUNTIME_LOG
            printf ( "look for element %s\n", strToken );
            fflush(stdout);
#endif
            prev_j = key_j;
            key_j = json_object_get( key_j, strToken);
            if(key_j == NULL)
            {
#ifdef RUNTIME_LOG
                printf("%s not found\n",strToken);
                fflush(stdout);
#endif
                break;
            }
            prevToken = strToken;
            strToken = strtok ( NULL, "." );
        }
        free(subpath);
    } 
    else if(strcmp(n->client->id,subject)==0)
    {
#ifdef RUNTIME_LOG
        printf("generate full tree with hostname for discover request\n");
        fflush(stdout);
#endif
        json_t *return_j = json_object();
        json_object_set( return_j, n->client->hostname, json_deep_copy(n->element));
        char * return_serialized = json_dumps(return_j, JSON_ENCODE_ANY);
        natsConnection_Publish(nc, natsMsg_GetReply(msg), (const void*) return_serialized, strlen(return_serialized));
        free(return_serialized);
        json_decref(return_j);
    }
    else
    {
#ifdef RUNTIME_LOG
        printf("request not handled\n");
        fflush(stdout);
#endif
    }

    pthread_mutex_unlock(&n->client->mutex);
    natsMsg_Destroy(msg);
}

bool NodeInitialize(Node *n)
{
    char *extendedPath = (char *)malloc(strlen(n->client->id) + strlen(".>") + 1);
    strcpy(extendedPath, n->client->id);
    strcat(extendedPath, ".>");

    pthread_mutex_lock(&n->client->mutex);
    natsConnection_Subscribe( &(n->sub[0]), n->client->client, n->client->id, MasterCb, (void *)n);
    natsConnection_Subscribe( &(n->sub[1]), n->client->client, extendedPath, MasterCb, (void *)n);
    pthread_mutex_unlock(&n->client->mutex);

    free(extendedPath);
    return true;
}

// Add a child node and notify Vbus
// Returns: a new node
Node *addNode(Node *n, char* id, char* string)
{
    pthread_mutex_lock(&n->client->mutex);
    char *newID = strdup(id);
    replace_char(newID, '.', '-');
    // create new node structure
    Node *node = (Node *)malloc(sizeof(Node));
    node->client = n->client;
    node->parent = n;
    node->path = (char *)malloc(strlen(n->path) + strlen(newID) + 2);
    strcpy(node->path, n->path);
    strcat(node->path, ".");
    strcat(node->path, newID);

    // fill node tree
    if(string == NULL)
    {
        node->element = json_object();
    }
    else
    {
        json_error_t error;
        node->element = json_loads(string, JSON_DECODE_ANY, &error);
        if(node->element==NULL)
        {
            node->element = json_object();
        }
    }

    // create simple parent struct
    json_t *parent_t = json_object();
    json_object_set_new( parent_t, newID, node->element);

    // publish
    char *path = (char *)malloc(strlen(n->path) + strlen(".add") + 1);
    strcpy(path, n->path);
    strcat(path, ".add");
    char * node_serialized = json_dumps(parent_t, JSON_ENCODE_ANY);
#ifdef RUNTIME_LOG
    printf("add new node: %s {%s}\n", node->path, node_serialized);
    fflush(stdout);
#endif
    natsConnection_Publish(n->client->client, path, (const void*) node_serialized, strlen(node_serialized));
    pthread_mutex_unlock(&n->client->mutex);

    json_decref(parent_t);
    free(node_serialized);
    free(path);
    free(newID);
    return node;
}

// Retrieve an existing node
// Returns: NULL is node doesn't exist
Node *getNode(Node *n, char* id)
{
    pthread_mutex_lock(&n->client->mutex);
    char *newID = strdup(id);
    replace_char(newID, '.', '-');
    json_t *element = json_object_get(n->element, newID);
    pthread_mutex_unlock(&n->client->mutex);
    if(element == NULL)
    {
        return NULL;
    }

    Node *node = (Node *)malloc(sizeof(Node));
    node->client = n->client;
    node->parent = n;
    node->element = element;
    node->path = (char *)malloc(strlen(n->path) + strlen(newID) + 2);
    strcpy(node->path, n->path);
    strcat(node->path, ".");
    strcat(node->path, newID);
    free(newID);

    return node;
}

void removeNode(Node *n)
{
    // publish
    pthread_mutex_lock(&n->client->mutex);
    char *path = (char *)malloc(strlen(n->path) + strlen(".remove") + 1);
    strcpy(path, n->path);
    strcat(path, ".remove");
#ifdef RUNTIME_LOG
    printf("remove node: %s\n", path);
    fflush(stdout);
#endif
    natsConnection_Publish(n->client->client, path, (const void*) NULL, 0);
    pthread_mutex_unlock(&n->client->mutex);
    free(path);
}

// Add a child attribute and notify Vbus
static json_t *attributeValue(void* value, char* type)
{
    json_t *value_t = NULL;

    if(strcmp(type,"integer") == 0)
    {
        value_t = json_integer((int)value);
    }
    else if(strcmp(type,"number") == 0)
    {
        value_t = json_real(*(double *)value);
    }
    else if(strcmp(type,"boolean") == 0)
    {
        value_t = json_boolean(value);
    }
    else if(strcmp(type,"string") == 0)
    {
        value_t = json_string((char *)value);
    }
    else
    {
#ifdef RUNTIME_LOG
        printf("attribute format %s not supported\n", type);
        fflush(stdout);
#endif
        return NULL;
    }

    return value_t;
}

Attribute *addAttribute(Node *n, char* id, void* value, char* type)
{
    // check we can understand value
    json_t *value_t = attributeValue(value, type);

    if(value_t == NULL)
    {
#ifdef RUNTIME_LOG
        printf("attribute value %s of type %s not supported\n", id, type);
        fflush(stdout);
#endif
        return NULL;
    }
    pthread_mutex_lock(&n->client->mutex);

    char *newID = strdup(id);
    replace_char(newID, '.', '-');
    // create new node structure
    Attribute *attribute = (Attribute *)malloc(sizeof(Attribute));
    attribute->client = n->client;
    attribute->parent = n;
    attribute->value = value_t;
    attribute->type = type;
    attribute->path = (char *)malloc(strlen(n->path) + strlen(newID) + 2);
    strcpy(attribute->path, n->path);
    strcat(attribute->path, ".");
    strcat(attribute->path, newID);

    // create simple parent struct
    json_t *parent_t = json_object();
    json_t *att_t = json_object();
    json_t *type_t = json_object();
    json_object_set_new( att_t, "value", value_t);
    json_object_set_new( type_t, "type", json_string(type));
    json_object_set_new( att_t, "schema", type_t);
    json_object_set_new( parent_t, newID, att_t);

    // publish
    char *path = (char *)malloc(strlen(n->path) + strlen(".add") + 1);
    strcpy(path, n->path);
    strcat(path, ".add");
    char * node_serialized = json_dumps(parent_t, JSON_ENCODE_ANY);
#ifdef RUNTIME_LOG
    printf("add new attribute: %s {%s}\n", attribute->path, node_serialized);
    fflush(stdout);
#endif
    natsConnection_Publish(n->client->client, path, (const void*) node_serialized, strlen(node_serialized));
    pthread_mutex_unlock(&n->client->mutex);

    json_decref(parent_t);
    free(node_serialized);
    free(path);
    free(newID);

    return attribute;
}

void setAttribute(Attribute *a, void* value)
{
    pthread_mutex_lock(&a->client->mutex);
    // marhsall value
    json_t *value_t = attributeValue(value, a->type);
    char * value_serialized = json_dumps(value_t, JSON_ENCODE_ANY);

    // publish
    char *path = (char *)malloc(strlen(a->path) + strlen(".value.set") + 1);
    strcpy(path, a->path);
    strcat(path, ".value.set");
#ifdef RUNTIME_LOG
    printf("set new attribute value: %s {%s}\n", path, value_serialized);
    fflush(stdout);
#endif
    natsConnection_Publish(a->client->client, path, (const void*) value_serialized, strlen(value_serialized));
    pthread_mutex_unlock(&a->client->mutex);
    free(value_serialized);
    free(path);
}

// generic cb 
static void methodGenericCb(natsConnection *nc, natsSubscription *sub, natsMsg *msg, void *closure)
{
    Method *method = (Method *)closure;

    method->cb((char *)natsMsg_GetData(msg), natsMsg_GetDataLength(msg), method->userdata);

    natsMsg_Destroy(msg);
}

// Add a child method node and notify Vbus
Method *addMethod(Node *n, char* id, method_cb cb, void *userdata)
{
    pthread_mutex_lock(&n->client->mutex);
    char *newID = strdup(id);
    replace_char(newID, '.', '-');
    // create new node structure
    Method *method = (Method *)malloc(sizeof(Method));
    method->client = n->client;
    method->parent = n;
    method->userdata = userdata;
    method->cb = cb;
    method->path = (char *)malloc(strlen(n->path) + strlen(newID) + 2);
    strcpy(method->path, n->path);
    strcat(method->path, ".");
    strcat(method->path, newID);


    // create simple parent struct
    json_t *parent_t = json_object();
    json_t *method_t = json_object();
    json_t *schema_t = json_object();
    json_t *type_t = json_object();
    json_t *return_t = json_object();
    json_object_set_new( schema_t, "type", json_string("object"));
    json_object_set_new( type_t, "type", json_string("object"));
    json_object_set_new( return_t, "return", type_t);
    json_object_set_new( schema_t, "properties", return_t);
    json_object_set_new( method_t, "schema", schema_t);
    json_object_set_new( parent_t, newID, method_t);

    // register subscribe
    char *subpath = (char *)malloc(strlen(method->path) + strlen(".set") + 1);
    strcpy(subpath, method->path);
    strcat(subpath, ".set");
    
    natsConnection_Subscribe( &(method->sub), method->client->client, subpath, methodGenericCb, (void *)method);

    // store sub path
    storeSubPath(n->client, subpath);

    // publish
    char *path = (char *)malloc(strlen(n->path) + strlen(".add") + 1);
    strcpy(path, n->path);
    strcat(path, ".add");
    char * node_serialized = json_dumps(parent_t, JSON_ENCODE_ANY);
#ifdef RUNTIME_LOG
    printf("add new node: %s {%s}\n", method->path, node_serialized);
    fflush(stdout);
#endif
    natsConnection_Publish(n->client->client, path, (const void*) node_serialized, strlen(node_serialized));
    pthread_mutex_unlock(&n->client->mutex);

    json_decref(parent_t);
    free(node_serialized);
    free(path);
    free(newID);
    return method;
}