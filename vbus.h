#ifndef VBUS_H
#define VBUS_H

#include <stdbool.h>

typedef struct Node_s Node;
typedef struct Attribute_s Attribute;
typedef struct Method_s Method;
typedef void (*method_cb)(char *data, int data_length, void *userdata);

Node *vBusNewClient(char *domain, char *id);

bool vBusConnect(Node * c);

Node *addNode(Node *n, char* id, char* string);
Node *getNode(Node *n, char* id);
void removeNode(Node *n);

Attribute *addAttribute(Node *n, char* id, void* value, char* type);
void setAttribute(Attribute *a, void* value);

Method *addMethod(Node *n, char* id, method_cb cb, void *userdata);

#endif
