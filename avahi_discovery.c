#include "vbus-private.h"
#include "timer/timer.h"

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <avahi-client/client.h>
#include <avahi-client/publish.h>
#include <avahi-client/lookup.h>
#include <avahi-common/alternative.h>
#include <avahi-common/simple-watch.h>
#include <avahi-common/malloc.h>
#include <avahi-common/error.h>

const AvahiPoll* ap = NULL;
static AvahiClient *client = NULL;
static AvahiEntryGroup *group = NULL;
static AvahiSimplePoll *simple_poll = NULL;
static AvahiServiceBrowser *service_browser = NULL;

static char *nats_path = NULL;

static void resolve_callback(
    AvahiServiceResolver *r,
    AVAHI_GCC_UNUSED AvahiIfIndex interface,
    AVAHI_GCC_UNUSED AvahiProtocol protocol,
    AvahiResolverEvent event,
    const char *name,
    const char *type,
    const char *domain,
    const char *host_name,
    const AvahiAddress *address,
    uint16_t port,
    AvahiStringList *txt,
    AvahiLookupResultFlags flags,
    AVAHI_GCC_UNUSED void* userdata) {
    assert(r);
    /* Called whenever a service has been resolved successfully or timed out */
    fprintf(stderr, "resolve_callback: event [%d]\n", event);

    switch (event) {
        case AVAHI_RESOLVER_FAILURE:
            fprintf(stderr, "(Resolver) Failed to resolve service '%s' of type '%s' in domain '%s': %s\n", name, type, domain, avahi_strerror(avahi_client_errno(avahi_service_resolver_get_client(r))));
            break;
        case AVAHI_RESOLVER_FOUND: {
            char a[AVAHI_ADDRESS_STR_MAX], *t;
            fprintf(stderr, "Service '%s' of type '%s' in domain '%s':\n", name, type, domain);
            avahi_address_snprint(a, sizeof(a), address);
            t = avahi_string_list_to_string(txt);
            fprintf(stderr,
                    "\t%s:%u (%s)\n"
                    "\tTXT=%s\n"
                    "\tcookie is %u\n"
                    "\tis_local: %i\n"
                    "\tour_own: %i\n"
                    "\twide_area: %i\n"
                    "\tmulticast: %i\n"
                    "\tcached: %i\n",
                    host_name, port, a,
                    t,
                    avahi_string_list_get_service_cookie(txt),
                    !!(flags & AVAHI_LOOKUP_RESULT_LOCAL),
                    !!(flags & AVAHI_LOOKUP_RESULT_OUR_OWN),
                    !!(flags & AVAHI_LOOKUP_RESULT_WIDE_AREA),
                    !!(flags & AVAHI_LOOKUP_RESULT_MULTICAST),
                    !!(flags & AVAHI_LOOKUP_RESULT_CACHED));

            if(!strcmp(type, "_nats._tcp"))
            {
                if(!strncmp(name, "vbus", strlen("vbus")))
                {
                    if(nats_path == NULL)
                    {
                        // next step compare host_name to choose the same one than the service if available
                        char str_port[6];
                        sprintf(str_port, "%d", port);
                        nats_path = malloc(strlen(a) + strlen(":") + strlen(str_port) + 1);
                        strcpy(nats_path, a);
                        strcat(nats_path, ":");
                        strcat(nats_path, str_port);
                        printf("vbus service found: %s\n", nats_path);
                        avahi_simple_poll_quit(simple_poll);
                    }
                }
            }

            avahi_free(t);
        }
    }
    avahi_service_resolver_free(r);
}

static void browse_callback(
    AvahiServiceBrowser *b,
    AvahiIfIndex interface,
    AvahiProtocol protocol,
    AvahiBrowserEvent event,
    const char *name,
    const char *type,
    const char *domain,
    AVAHI_GCC_UNUSED AvahiLookupResultFlags flags,
    void* userdata) {
    AvahiClient *c = userdata;
    assert(b);

    /* Called whenever a new services becomes available on the LAN or is removed from the LAN */
    fprintf(stderr, "resolve_callback: event [%d]\n", event);

    switch (event) {
        case AVAHI_BROWSER_FAILURE:
            fprintf(stderr, "(Browser) %s\n", avahi_strerror(avahi_client_errno(avahi_service_browser_get_client(b))));
            avahi_simple_poll_quit(simple_poll);
            return;
        case AVAHI_BROWSER_NEW:
            fprintf(stderr, "(Browser) NEW: service '%s' of type '%s' in domain '%s'\n", name, type, domain);
            /* We ignore the returned resolver object. In the callback
               function we free it. If the server is terminated before
               the callback function is called the server will free
               the resolver for us. */
            if (!(avahi_service_resolver_new(c, interface, protocol, name, type, domain, AVAHI_PROTO_UNSPEC, 0, resolve_callback, c)))
                fprintf(stderr, "Failed to resolve service '%s': %s\n", name, avahi_strerror(avahi_client_errno(c)));
            break;
        case AVAHI_BROWSER_REMOVE:
            fprintf(stderr, "(Browser) REMOVE: service '%s' of type '%s' in domain '%s'\n", name, type, domain);
            break;
        case AVAHI_BROWSER_ALL_FOR_NOW:
        case AVAHI_BROWSER_CACHE_EXHAUSTED:
            fprintf(stderr, "(Browser) %s\n", event == AVAHI_BROWSER_CACHE_EXHAUSTED ? "CACHE_EXHAUSTED" : "ALL_FOR_NOW");
            break;
    }
}


/**********************************************************************************************************************
*
*   Function     : client_callback
*
*	Description  : Callback function for an Avahi client, called whenever there is a change in the client state.
*                  This is also called when the Avahi client is first created by the avahi_client_new() function.
*                  The pointer returned by avahi_client_new() is used to setup an entry group static pointer but
*                  could also be initialised here, in a similar way to the entry_group_callback() function.
*                  One scheme should be chosen, it may be better to set and clear static pointers in one place.
*                  The only problem would be if the callback calls any functions that make use of static pointers
*                  but it should be possible to avoid this by passing these as arguments to any function calls.
*                  The static pointers should really only be used in places where this really is unavoidable.
*
*                  The service browser can only be created once the client is running. If Avahi is running when
*                  the service manager starts, the browser is created in the context of avahi_client_new(). If
*                  Avahi starts after the service manager runs then the browser is created in the simple poll.
*                  Either way, the service browser is created here on change of state to AVAHI_CLIENT_S_RUNNING.
*
*   Documentation: https://www.avahi.org/doxygen/html/client_8h.html#a07b2a33a3e7cbb18a0eb9d00eade6ae6
*
**********************************************************************************************************************/

// use the argument pointer rather than the static wherever  possible
static void client_callback(AvahiClient *client_in, AvahiClientState state, AVAHI_GCC_UNUSED void * userdata) {
    assert(client_in);

    fprintf(stderr, "client_callback: client_in [%p] client [%p] state [%d]\n",
                    (void*) client_in, (void*) client, state);

    /* Called whenever the client or server state changes */
    switch (state) {
        case AVAHI_CLIENT_S_RUNNING:
            fprintf(stderr, "Client running...\n");

            /*
             * The Avahi server (and client) is now running, register the service browser
             */
            service_browser = avahi_service_browser_new(client_in,
                                           AVAHI_IF_UNSPEC,
                                           AVAHI_PROTO_UNSPEC,
                                           "_nats._tcp",
                                           NULL,
                                           0,
                                           browse_callback,
                                           client_in);
            if (! service_browser) {
                fprintf(stderr, "Failed to create service browser: %s\n", avahi_strerror(avahi_client_errno(client_in)));
                avahi_simple_poll_quit(simple_poll);
                break;
            }

            fprintf(stderr, "Created service browser [%p]\n", (void*)service_browser);

            break;
        case AVAHI_CLIENT_FAILURE:
            fprintf(stderr, "Client failure: %s\n", avahi_strerror(avahi_client_errno(client_in)));
            avahi_simple_poll_quit(simple_poll);
            break;
        case AVAHI_CLIENT_S_COLLISION:
            /* Let's drop our registered services. When the server is back
             * in AVAHI_SERVER_RUNNING state we will register them
             * again with the new host name. */
            fprintf(stderr, "Client collision...\n");
            break;
        case AVAHI_CLIENT_S_REGISTERING:
            /* The server records are now being established. This
             * might be caused by a host name change. We need to wait
             * for our own records to register until the host name is
             * properly esatblished. */
            fprintf(stderr, "Client registering...\n");
            if (group)
                avahi_entry_group_reset(group);
            break;
        case AVAHI_CLIENT_CONNECTING:
            fprintf(stderr, "Client connecting...\n");
            break;
    }
}

void avahi_close(void)
{
    avahi_simple_poll_quit(simple_poll);
}

char *avahi_interface_init( void) {
    AvahiClient *free_client = NULL;
    AvahiSimplePoll *free_simple_poll = NULL;

    int error;

    if(nats_path != NULL)
    {
        free(nats_path);
        nats_path = NULL;
    }

    /* Allocate main loop object */
    simple_poll = avahi_simple_poll_new();

    if (! simple_poll) {
        fprintf(stderr, "Failed to create Avahi poll\n");
        goto fail;
    }
    
    ap = avahi_simple_poll_get(simple_poll);
    /* Allocate a new client */
    client = avahi_client_new(ap,
                                AVAHI_CLIENT_NO_FAIL,  // stay in the loop until Avahi available
                                client_callback,
                                NULL,
                                &error);

    /* Check whether creating the client object succeeded */
    if (! client) {
        fprintf(stderr, "Failed to create Avahi client: %s\n", avahi_strerror(error));
        avahi_simple_poll_free(simple_poll);
        goto fail;
    }

    fprintf(stderr, "Created Avahi poll client, state [%d].\n", avahi_client_get_state(client));

    fprintf(stderr, "Entering Avahi poll...\n");

    // start timer in case we never get a good answer from avahi
    start_timer(10000, avahi_close);

    avahi_simple_poll_loop(simple_poll);

    stop_timer();

    // Exiting loop, loss of communication with Avahi server?
    free_client      = client;       // local copy to free
    free_simple_poll = simple_poll;  // local copy to free

    client = 0;          // clear static pointer
    simple_poll = 0;     // clear static pointer
    service_browser = 0; // clear static pointer - released by avahi_client_free()
    group = 0;           // clear static pointer - released by avahi_client_free()

    fprintf(stderr, "Exiting Avahi poll...\n");

    fprintf(stderr, "Freeing client [%p]\n", free_client);
    avahi_client_free(free_client);

    fprintf(stderr, "Freeing simple poll [%p]\n", free_simple_poll);
    avahi_simple_poll_free(free_simple_poll);

fail:
    fprintf(stderr, "Starting cleanup\n");

    /* Cleanup things */

    // service browsers and group entries also released by avahi_client_free()
    if (client) {
        avahi_client_free(client);
        client = 0;
    }
    if (simple_poll) {
        avahi_simple_poll_free(simple_poll);
        simple_poll = 0;
    }

    fprintf(stderr, "Cleanup complete\n");
    
    return nats_path;
}
