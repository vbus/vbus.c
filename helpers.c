#include "vbus-private.h"

#include <dbus/dbus.h>
#include <time.h>

const char * getHostname(void)
{
    const char *hostname;
    // from http://www.matthew.ath.cx/projects.git/dbus-example.c
    DBusError err;
    DBusConnection* conn;
    DBusMessage* msg;
    DBusPendingCall* pending;
    DBusMessageIter args;

    // initialise the errors
    dbus_error_init(&err);
    // connect to the bus
    conn = dbus_bus_get(DBUS_BUS_SYSTEM, &err);
    if (dbus_error_is_set(&err)) { 
        fprintf(stderr, "Connection Error (%s)\n", err.message); 
        dbus_error_free(&err); 
    }
    if (NULL == conn) { 
        exit(1); 
    }

    // create a new method call and check for errors
    msg = dbus_message_new_method_call("io.veea.VeeaHub.Info", // target for the method call
                                        "/io/veea/VeeaHub/Info", // object to call on
                                        "io.veea.VeeaHub.Info", // interface to call on
                                        "Hostname"); // method name
    if (NULL == msg) { 
        fprintf(stderr, "Message Null\n");
        exit(1);
    }

    // send message and get a handle for a reply
    if (!dbus_connection_send_with_reply (conn, msg, &pending, -1)) { // -1 is default timeout
        fprintf(stderr, "Out Of Memory!\n"); 
        exit(1);
    }
    if (NULL == pending) { 
        fprintf(stderr, "Pending Call Null\n"); 
        exit(1); 
    }
    dbus_connection_flush(conn);

    printf("Request Sent\n");

    // free message
    dbus_message_unref(msg);
    printf("Request Sent\n");

    // block until we recieve a reply
    dbus_pending_call_block(pending);

    // get the reply message
    msg = dbus_pending_call_steal_reply(pending);
    if (NULL == msg) {
        fprintf(stderr, "Reply Null\n"); 
        exit(1); 
    }
    // free the pending message handle
    dbus_pending_call_unref(pending);

    // read the parameters
    if (!dbus_message_iter_init(msg, &args))
        fprintf(stderr, "Message has no arguments!\n"); 
    else if (DBUS_TYPE_STRING != dbus_message_iter_get_arg_type(&args)) 
        fprintf(stderr, "Argument is not string!\n"); 
    else {
        dbus_message_iter_get_basic(&args, &hostname);
        printf("Got Signal with value %s\n", hostname);
    }

    // free reply and close connection
    dbus_message_unref(msg);   
    dbus_connection_unref(conn);

	return hostname;
}

char * genPassword( void) 
{
	char ch[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@$#%^&*()";
	char *b  = malloc(22);
    int max = strlen(ch);
	srand(time(NULL));

    int i;
	for (i=0; i<22; i++) 
    {
		int randnum = rand();
		b[i] = ch[randnum%max];
	}
    b[22] = 0;
	return b;
}

char* replace_char(char* str, char find, char replace)
{
    char *current_pos = strchr(str,find);
    while (current_pos){
        *current_pos = replace;
        current_pos = strchr(current_pos,find);
    }
    return str;
}