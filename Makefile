CC = gcc
LDFLAGS :=  -lc \
						-ljansson \
						-lnats \
						-lavahi-client \
						-lavahi-core \
						-lavahi-common
ARM64LIBPATH = /usr/lib/aarch64-linux-gnu
ARMHFLIBPATH = /usr/lib/arm-linux-gnueabihf
X86LIBPATH = /usr/lib/x86_64-linux-gnu

CFLAGS := -O0 -g -c -Wall -I. -std=gnu99\
`pkg-config dbus-1 --cflags` \
`pkg-config glib-2.0 --cflags`

PROJECT := libvbus.a
CP       = cp
OBJS := timer/timer.o \
		crypt_blowfish-1.3/crypt_blowfish.o \
		crypt_blowfish-1.3/crypt_gensalt.o \
		crypt_blowfish-1.3/wrapper.o \
		avahi_discovery.o \
		node.o \
		nats.o \
		helpers.o \
		client.o

armhf: $(OBJS)
	ar -x /usr/local/lib/libnats_static.a 
	ar -x $(ARMHFLIBPATH)/libavahi-client.a
	ar -x $(ARMHFLIBPATH)/libjansson.a
	ar -x $(ARMHFLIBPATH)/libavahi-common.a
	ar qcs $(PROJECT) *.o

arm64: $(OBJS)
	ar -x /usr/local/lib/libnats_static.a 
	ar -x $(ARM64LIBPATH)/libavahi-client.a
	ar -x $(ARM64LIBPATH)/libjansson.a
	ar -x $(ARM64LIBPATH)/libavahi-common.a
	ar qcs $(PROJECT) *.o

armhf_alpine: $(OBJS)
	ar -x /usr/local/lib/libnats_static.a 
	ar -x /usr/lib/libavahi-client.a
	ar -x /usr/lib/libjansson.a
	ar -x /usr/lib/libavahi-common.a
	ar qcs $(PROJECT) $(OBJS)  *.o

arm64_alpine: $(OBJS)
	ar -x /usr/local/lib64/libnats_static.a 
	ar -x /usr/lib/libavahi-client.a
	ar -x /usr/lib/libjansson.a
	ar -x /usr/lib/libavahi-common.a
	ar qcs $(PROJECT) $(OBJS) *.o

x86: $(OBJS)
	ar -x /usr/local/lib/libnats_static.a 
	ar -x $(X86LIBPATH)/libavahi-client.a
	ar -x $(X86LIBPATH)/libjansson.a
	ar -x $(X86LIBPATH)/libavahi-common.a
	ar qcs $(PROJECT) $(OBJS) *.o 

%.o: %.c Makefile
	$(CC) $(CFLAGS) -c $< -o $@

clean:	
	rm $(OBJS) $(PROJECT)

install:
	$(CP) $(PROJECT) /usr/local/lib
	$(CP) vbus.h /usr/local/include