#include "vbus-private.h"


Node *vBusNewClient(char *domain, char *id)
{
    // create vbus element struct
    ExtendedNatsClient *nats = NewExtendedNatsClient(domain, id);
    return NewMasterNode(nats);
}

bool vBusConnect(Node * c)
{
    bool result = ExtendedConnect(c->client);
    if(result == false)
    {
        return false;
    }

    return NodeInitialize(c);
}

bool vBusPermission(Node * c, char *permission)
{

}